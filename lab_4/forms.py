from django.db.models.fields import CharField, TextField
from django.forms.widgets import TextInput
from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
    message = forms.CharField(widget=forms.Textarea(attrs={'cols': 40, 'rows': 10, 'placeholder':'message'}))