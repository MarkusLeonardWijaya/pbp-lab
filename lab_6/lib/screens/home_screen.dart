import 'package:flutter/material.dart';
import 'package:lab_6/widgets/kartu.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: const Text("BIMBOL • Bimbel Online Di Masa Pandemi"),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      body: ListView(
        children: [
          Container(
            alignment: Alignment.topCenter,
            color: Color(0xff171941),
            child: const Text(
              'Forum Diskusi',
              style: TextStyle(
                height: 2,
                fontWeight: FontWeight.bold,
                fontSize: 40,
                fontFamily: 'DancingScript1',
                color: Colors.white70,
              ),
            ),
          ),
          const Kartu("AVL Trees with Implementation in C++, Java, and Python"),
          const SizedBox(height: 8),
          const Kartu(
              "A Material Design app bar. An app bar consists of a toolbar and potentially other widgets, such as a TabBar and a FlexibleSpaceBar."),
          const SizedBox(height: 8),
          const Kartu(
              "Layout a list of child widgets in the vertical direction."),
          const SizedBox(height: 8),
          const Kartu(
              "The Flutter logo, in widget form. This widget respects the IconTheme."),
          const SizedBox(height: 8),
          const Kartu("A Material Design icon."),
          // Kartu(),
          // Kartu(),
          // Kartu(),
          // Kartu(),
          // Kartu(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
    );
  }
}
