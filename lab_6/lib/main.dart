import 'package:flutter/material.dart';
import 'package:vertical_card_pager/vertical_card_pager.dart';

import 'screens/home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab_6',
      theme: ThemeData(
        primaryColor: const Color(0xff171941),
        secondaryHeaderColor: const Color(0xffe14eca),
      ),
      home: HomeScreen(),
    );
  }
}
