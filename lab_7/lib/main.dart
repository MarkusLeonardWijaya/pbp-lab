import 'package:flutter/material.dart';
import 'package:lab_7/screens/home_screen.dart';
import 'package:lab_7/screens/add_forum.dart';

import 'screens/add_forum.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab_7',
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/add_forum': (context) => const AddForum(),
      },
      theme: ThemeData(
        primaryColor: const Color(0xff171941),
        secondaryHeaderColor: const Color(0xffe14eca),
      ),
    );
  }
}
