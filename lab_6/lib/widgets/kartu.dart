import 'package:flutter/material.dart';

class Kartu extends StatelessWidget {
  final String judul;

  const Kartu(this.judul);

  @override
  Widget build(BuildContext context) {
    print(judul);
    return Center(
      child: Card(
        color: Color(0xff1f2251),
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            print('Card tapped.');
          },
          child: SizedBox(
            width: 300,
            height: 100,
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Text(
                judul,
                style: const TextStyle(
                  fontSize: 20,
                  color: Colors.white70,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
