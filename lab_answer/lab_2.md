## 1. Apa perbedaan XML dan json?

| XML         | json        |
| :---:        |    :----:   |
| Data disimpan sebagai tree structure | Data disimpan seperti map dengan pasangan key-value |
| Menggunakan tags seperti HTML (menggunakan kurung sudut) | Tidak menggunakan tags (menggunakan kurung kurawal, ada key dan ada value)   |
|Dapat dituliskan comment | Tidak dapat dituliskan comment|
|Ukuran file lebih besar|Ukuran file lebih kecil|
|Syntax lebih rumit|Syntax lebih mudah dimengerti|
Memiliki tipe data string (yang nantinya dapat terkonversi menjadi gambar, chart, dan data-data lainnya)|Memiliki tipe data array, string, number, boolean

## 2. Apakah perbedaan antara HTML dan XML?\

| XML         | HTML        |
| :---:        |    :----:   |
|menitikberatkan pada bagaimana format tampilan dari data | menitikberatkan pada struktur dan konteksnya|
|tag pada XML bersifat case sensiitive| tag pada HTML bersiifat case insensitive|
|XML menyediakan dukungan namespaces | HTML tidak menyediakan dukungan namespaces|
Tag XML dapat dikembangkan | HTML memiliki tag terbatas