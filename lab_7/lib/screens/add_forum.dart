import 'package:flutter/material.dart';
import 'package:lab_7/widgets/alert_box.dart';

class AddForum extends StatefulWidget {
  const AddForum({Key key}) : super(key: key);

  @override
  State<AddForum> createState() => _AddForum();
}

var judul;

class _AddForum extends State<AddForum> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text("BIMBOL • Bimbel Online Di Masa Pandemi"),
        backgroundColor: Theme.of(context).secondaryHeaderColor,
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              // TextField(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    labelText: "Judul Forum",
                    labelStyle: TextStyle(color: Colors.white54),
                    icon: Icon(
                      Icons.title_rounded,
                      color: Colors.white54,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 1.0,
                        style: BorderStyle.solid,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 1.0,
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                  validator: (value) {
                    judul = value;
                    if (value.isEmpty) {
                      return 'Judul tidak boleh kosong';
                    }
                    print(value);
                  },
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                    labelText: "Deskripsi",
                    labelStyle: TextStyle(color: Colors.white54),
                    icon: Icon(
                      Icons.people,
                      color: Colors.white54,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 1.0,
                        style: BorderStyle.solid,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 1.0,
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'deskripsi tidak boleh kosong';
                    }
                    print(value);
                  },
                ),
              ),

              // TextFormField(
              //   decoration: InputDecoration(
              //     labelText: "Judul Forum",
              //     labelStyle: TextStyle(color: Colors.white),
              //     border: OutlineInputBorder(
              //       borderRadius: BorderRadius.circular(5.0),
              //       borderSide: BorderSide(
              //         color: Colors.white,
              //         width: 1.0,
              //         style: BorderStyle.solid,
              //       ),
              //     ),
              //   ),
              //   validator: (value) {
              //     judul = value;
              //     if (value.isEmpty) {
              //       return 'judul tidak boleh kosong';
              //     }
              //     print(value);
              //   },
              // ),

              // const SizedBox(height: 12),

              // TextFormField(
              //   decoration: InputDecoration(
              //     labelText: "isi forum...",
              //     labelStyle: TextStyle(color: Colors.white),
              //     border: OutlineInputBorder(
              //       borderRadius: BorderRadius.circular(5.0),
              //       borderSide: BorderSide(
              //         color: Colors.white,
              //         width: 1.0,
              //         style: BorderStyle.solid,
              //       ),
              //     ),
              //   ),
              //   validator: (value) {
              //     desc = value;
              //     print(value);
              //     if (value.isEmpty) {
              //       return 'deskripsi tidak boleh kosong';
              //     }
              //   },
              // ),

              // const SizedBox(height: 12),

              // AlertBox(judul, desc, _formKey.currentState.validate()),

              ElevatedButton(
                onPressed: () {
                  print(_formKey);
                  if (_formKey.currentState.validate()) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('Succes!!'),
                        content: Text(
                            'Forum $judul berhasil ditambahkan, silahkan cek di halaman Forum Diskusi'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'OK'),
                            child: const Text('OK'),
                          ),
                        ],
                      ),
                    );
                  }
                },
                child: const Text(
                  'Submit',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
