// NGGAK KEPAKE
import 'package:flutter/material.dart';
import 'package:lab_7/screens/add_forum.dart';

class AlertBox extends StatelessWidget {
  final judul, desc;
  final _formKey;

  const AlertBox(this.judul, this.desc, this._formKey);

  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        print(_formKey);
        if (_formKey != null || _formKey) {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: const Text('Succes!!'),
              content: const Text(
                  'Forum berhasil ditambahkan, silahkan cek di halaman Forum Diskusi'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text('OK'),
                ),
              ],
            ),
          );
        }
      },
      child: const Text(
        'Submit',
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
